import { Module } from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';
import { ServeStaticModule } from '@nestjs/serve-static';
import { TypegooseModule } from 'nestjs-typegoose';
import { join } from 'path';
import { User } from 'src/models/user.model';
import { AppController } from './app.controller';
import { MONGO_URI } from './config';
import { AuthGuard } from './guards/auth.guard';
import { UploadsModule } from './modules/uploads/uploads.module';
import { UsersModule } from './modules/users/users.module';
import { StatesModule } from './modules/states/states.module';
import { CarsModule } from './modules/cars/cars.module';
import { ReservesModule } from './modules/reserves/reserves.module';

const TypegooseConnection = TypegooseModule.forRoot(MONGO_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: false,
});

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    TypegooseConnection,
    TypegooseModule.forFeature([User]),
    UploadsModule,
    UsersModule,
    StatesModule,
    CarsModule,
    ReservesModule,
  ],
  controllers: [AppController],
  providers: [{ provide: APP_GUARD, useClass: AuthGuard }],
})
export class AppModule {}
