export const swaggerCustomCss = `
.swagger-ui .topbar .download-url-wrapper {
  display: none
}

@font-face {
  font-family: Font;
  src: url(https://cdn.rawgit.com/rastikerdar/vazir-font/v21.2.1/dist/Vazir.eot);
  src: url(https://cdn.rawgit.com/rastikerdar/vazir-font/v21.2.1/dist/Vazir.eot?#iefix) format("embedded-opentype"), url(https://cdn.rawgit.com/rastikerdar/vazir-font/v21.2.1/dist/Vazir.woff2) format("woff2"), url(https://cdn.rawgit.com/rastikerdar/vazir-font/v21.2.1/dist/Vazir.woff) format("woff"), url(https://cdn.rawgit.com/rastikerdar/vazir-font/v21.2.1/dist/Vazir.ttf) format("truetype");
  font-weight: 400;
}

h1,
h2,
h3,
h4,
h5,
h6,
pre,
div,
p,
a,
span,
input,
select,
option {
  font-family: Font !important;
}

.swagger-ui .opblock .opblock-summary-description {
  text-align: right;
}

h2.title,
div.description,
select {
  direction: rtl;
}

p {
  margin: 0 !important;
}

.swagger-ui .info .title small {
  margin: 0 5px 0 0 !important;
}
`;

export const swaggerTitle = 'راهنمای وب سرویس';
