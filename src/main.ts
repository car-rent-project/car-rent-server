import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { join } from 'path';
import { AppModule } from './app.module';
import { swaggerCustomCss, swaggerTitle } from './doc-config';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule, {
    cors: true,
  });
  app.enableCors();
  app.useStaticAssets(join(__dirname, '..', 'public'));

  const options = new DocumentBuilder()
    .setTitle(swaggerTitle)
    .setVersion('نسخه توسعه')
    .addBearerAuth()
    .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('docs', app, document, {
    customSiteTitle: swaggerTitle,
    customCss: swaggerCustomCss,
    swaggerOptions: {
      docExpansion: 'none',
      layout: 'BaseLayout',
      tagsSorter: 'alpha',
      operationsSorter: (a: any, b: any) => {
        const sortArray = ['get', 'post', 'put', 'patch', 'delete'];

        const aMethod = a._root.entries.find((e: any) => e[0] === 'method')[1];
        const bMethod = b._root.entries.find((e: any) => e[0] === 'method')[1];

        return sortArray.indexOf(aMethod) <= sortArray.indexOf(bMethod)
          ? -1
          : 1;
      },
    },
  });
  await app.listen(process.env.PORT || 3000);
}
bootstrap();
