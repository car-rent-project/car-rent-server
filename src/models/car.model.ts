import { ApiProperty } from '@nestjs/swagger';
import { ModelOptions, prop, Ref } from '@typegoose/typegoose';
import { BaseSchemaTimeStamp } from '../core/baseSchema';
import { State } from './state.model';
import { User } from './user.model';

export const CAR_TYPE = ['sedan', 'truck'];

@ModelOptions({ schemaOptions: { timestamps: true } })
export class Car extends BaseSchemaTimeStamp {
  @ApiProperty({ description: 'نمایش', default: true })
  @prop({ default: true })
  visible: boolean;

  @ApiProperty({ type: State, description: 'شناسه شهر' })
  @prop({ ref: State })
  state: Ref<State>;

  @ApiProperty({ type: User, description: 'شناسه کاربر' })
  @prop({ ref: User })
  user: Ref<User>;

  @ApiProperty({ description: 'تصاویر خودرو' })
  @prop({ type: String })
  images: string[];

  @ApiProperty({ description: 'نام خودرو' })
  @prop()
  title: string;

  @ApiProperty({ enum: CAR_TYPE, description: 'نوع خودرو' })
  @prop({ enum: CAR_TYPE })
  type: string;
}
