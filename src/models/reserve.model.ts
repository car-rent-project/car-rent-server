import { ApiProperty } from '@nestjs/swagger';
import { ModelOptions, prop, Ref } from '@typegoose/typegoose';
import { BaseSchemaTimeStamp } from '../core/baseSchema';
import { Car } from './car.model';
import { User } from './user.model';

@ModelOptions({ schemaOptions: { timestamps: true } })
export class Reserve extends BaseSchemaTimeStamp {
  @ApiProperty({ type: Car, description: 'شناسه خودرو' })
  @prop({ ref: Car })
  car: Ref<Car>;

  @ApiProperty({ type: User, description: 'شناسه کاربر' })
  @prop({ ref: User })
  user: Ref<User>;

  @ApiProperty({ description: 'تایید شده' })
  @prop()
  accepted: boolean;
}
