import { ApiProperty } from '@nestjs/swagger';
import { prop, Ref } from '@typegoose/typegoose';
import { BaseSchema } from './../core/baseSchema';

export const STATE_TYPE = ['province', 'city'];

export class State extends BaseSchema {
  @ApiProperty({ type: State })
  @prop({ ref: State })
  parent?: Ref<State>;

  @ApiProperty()
  @prop({ required: true })
  title?: string;

  @ApiProperty({ required: true, enum: STATE_TYPE })
  @prop({ required: true })
  type?: string;
}
