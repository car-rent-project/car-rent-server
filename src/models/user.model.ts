import { ApiProperty } from '@nestjs/swagger';
import { ModelOptions, prop } from '@typegoose/typegoose';
import { BaseSchemaTimeStamp } from 'src/core/baseSchema';
import Validator from 'validator';

const USER_STATUS = ['active', 'inactive', 'block'];
const USER_ROLE = ['admin', 'user'];

class UserKeys {
  @prop()
  activateKey?: string;
  @prop()
  activateExpire?: Date;
  @prop()
  resetPasswordKey?: string;
  @prop()
  resetPasswordExpire?: Date;
}

@ModelOptions({ schemaOptions: { timestamps: true } })
export class User extends BaseSchemaTimeStamp {
  @ApiProperty({ required: true })
  @prop({ required: true, unique: true })
  phone: string;

  @ApiProperty({ required: false })
  @prop()
  name?: string;

  @ApiProperty({ required: false })
  @prop()
  avatar?: string;

  @ApiProperty({
    required: false,
    enum: USER_STATUS,
    default: 'inactive',
  })
  @prop({ type: String, enum: USER_STATUS, default: 'inactive' })
  status?: string;

  @ApiProperty({
    required: false,
    enum: USER_ROLE,
    default: 'user',
  })
  @prop({ type: String, enum: USER_ROLE, default: 'user' })
  role?: string;

  @prop()
  keys?: UserKeys;
}
