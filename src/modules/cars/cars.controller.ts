import { User } from 'src/models/user.model';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  Req,
} from '@nestjs/common';
import {
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiProperty,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { ReturnModelType } from '@typegoose/typegoose';
import { InjectModel } from 'nestjs-typegoose';
import { Auth } from './../../guards/auth.guard';
import { Car, CAR_TYPE } from './../../models/car.model';
import { userInfo } from 'os';

class CarsResponse {
  @ApiProperty({ readOnly: true, type: [Car] })
  cars: Car[];

  @ApiProperty()
  count: number;
}

class CarResponse {
  @ApiProperty({ readOnly: true, type: Car })
  car: Car;
}

@ApiTags('Cars')
@Controller('api/cars')
export class CarsController {
  constructor(
    @InjectModel(Car) private readonly carModel: ReturnModelType<typeof Car>,
  ) {}

  //#region Get by id
  @ApiOperation({ summary: 'دریافت خودرو با شناسه' })
  @ApiParam({ name: 'id', description: 'شناسه' })
  @ApiOkResponse({ type: CarResponse })
  @Get(':id')
  async getById(@Param('id') id: string) {
    const car = await this.carModel.findById(id);
    return { car };
  }

  //#endregion
  //#region Get list
  @ApiOperation({ summary: 'دریافت لیست خودرو ها' })
  @ApiQuery({ name: 'search', required: false, description: 'جست و جو' })
  @ApiQuery({ name: 'state', required: false, description: 'شناسه شهر' })
  @ApiQuery({
    name: 'type',
    required: false,
    description: 'نوع خودرو',
    enum: CAR_TYPE,
    examples: {
      sedan: { value: 'sedan', summary: 'سواری' },
      truck: { value: 'truck', summary: 'باربری' },
    },
  })
  @ApiQuery({ name: 'skip', required: false })
  @ApiQuery({ name: 'limit', required: false })
  @ApiOkResponse({ type: CarsResponse })
  @Get()
  async getList(
    @Query('search') search: string,
    @Query('state') state: string,
    @Query('type') type: string,
    @Query('skip') skip: number,
    @Query('limit') limit: number,
  ) {
    const cond = {};
    if (!!search) Object.assign(cond, { name: new RegExp(search, 'ig') });
    if (!!type) Object.assign(cond, { type });
    if (!!state) Object.assign(cond, { state });

    const query = this.carModel.find(cond);
    if (!!skip) query.skip(skip);
    if (!!limit) query.limit(limit);

    const cars = await query.exec();
    const count = await this.carModel.count(cond);
    return { cars, count };
  }

  //#endregion
  //#region Create
  @ApiOperation({ summary: 'ایجاد خودرو' })
  @ApiOkResponse({ type: CarResponse })
  @Auth()
  @Post()
  async create(@Body() data: Car, @Req() req: { user: User }) {
    const car = new this.carModel({ ...data, user: req.user._id });
    await car.save();
    return { car };
  }

  //#endregion
  //#region Update
  @ApiOperation({ summary: 'بروزرسانی خودرو با شناسه' })
  @ApiParam({ name: 'id', description: 'شناسه' })
  @ApiOkResponse({ type: CarResponse })
  @Auth()
  @Put(':id')
  async update(@Param('id') id: string, @Body() data: Car) {
    const car = await this.carModel.findByIdAndUpdate(id, data, { new: true });
    return { car };
  }

  //#endregion
  //#region Delete
  @ApiOperation({ summary: 'حذف خودرو با شناسه' })
  @ApiParam({ name: 'id', description: 'شناسه' })
  @ApiOkResponse({ type: CarResponse })
  @Auth()
  @Delete(':id')
  async delete(@Param('id') id: string) {
    const car = await this.carModel.findByIdAndRemove(id);
    return { car };
  }

  //#endregion
}
