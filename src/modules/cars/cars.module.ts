import { TypegooseModule } from 'nestjs-typegoose';
import { Car } from './../../models/car.model';
import { Module } from '@nestjs/common';
import { CarsController } from './cars.controller';

@Module({
  imports: [TypegooseModule.forFeature([Car])],
  controllers: [CarsController],
})
export class CarsModule {}
