import { User } from 'src/models/user.model';
import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
  Req,
} from '@nestjs/common';
import {
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiProperty,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { ReturnModelType } from '@typegoose/typegoose';
import { InjectModel } from 'nestjs-typegoose';
import { Auth } from './../../guards/auth.guard';
import { Reserve } from './../../models/reserve.model';

class ReservesResponse {
  @ApiProperty({ readOnly: true, type: [Reserve] })
  reserves: Reserve[];

  @ApiProperty()
  count: number;
}

class ReserveResponse {
  @ApiProperty({ readOnly: true, type: Reserve })
  reserve: Reserve;
}

@ApiTags('Reserves')
@Controller('api/reserves')
export class ReservesController {
  constructor(
    @InjectModel(Reserve)
    private readonly reserveModel: ReturnModelType<typeof Reserve>,
  ) {}

  //#region Get by id
  @ApiOperation({ summary: 'دریافت رزرو با شناسه' })
  @ApiParam({ name: 'id', description: 'شناسه' })
  @ApiOkResponse({ type: ReserveResponse })
  @Get(':id')
  async getById(@Param('id') id: string) {
    const reserve = await this.reserveModel.findById(id);
    return { reserve };
  }

  //#endregion
  //#region Get list
  @ApiOperation({ summary: 'دریافت لیست رزرو ها' })
  @ApiQuery({ name: 'search', required: false, description: 'جست و جو' })
  @ApiQuery({ name: 'user', required: false, description: 'شناسه کاربر' })
  @ApiQuery({ name: 'car', required: false, description: 'شناسه خودرو' })
  @ApiQuery({ name: 'skip', required: false })
  @ApiQuery({ name: 'limit', required: false })
  @ApiOkResponse({ type: ReservesResponse })
  @Get()
  async getList(
    @Query('search') search: string,
    @Query('user') user: string,
    @Query('car') car: string,
    @Query('skip') skip: number,
    @Query('limit') limit: number,
  ) {
    const cond = {};
    if (!!search) Object.assign(cond, { name: new RegExp(search, 'ig') });
    if (!!car) Object.assign(cond, { car });
    if (!!user) Object.assign(cond, { user });

    const query = this.reserveModel.find(cond);
    if (!!skip) query.skip(skip);
    if (!!limit) query.limit(limit);

    const reserves = await query.exec();
    const count = await this.reserveModel.count(cond);
    return { reserves, count };
  }

  //#endregion
  //#region Create
  @ApiOperation({ summary: 'ایجاد رزرو' })
  @ApiOkResponse({ type: ReserveResponse })
  @Auth()
  @Post()
  async create(@Body() data: Reserve, @Req() req: { user: User }) {
    const reserveExists = await this.reserveModel.exists({
      car: data.car,
      accepted: true,
    });
    if (!!reserveExists) {
      throw new HttpException(
        { message: 'این خودرو رزرو قبلا رزرو شده است' },
        HttpStatus.CONFLICT,
      );
    }

    const reserve = new this.reserveModel({ ...data, user: req.user._id });
    await reserve.save();
    return { reserve };
  }

  //#endregion
  //#region Update
  @ApiOperation({ summary: 'بروزرسانی رزرو با شناسه' })
  @ApiParam({ name: 'id', description: 'شناسه' })
  @ApiOkResponse({ type: ReserveResponse })
  @Auth()
  @Put(':id')
  async update(@Param('id') id: string, @Body() data: Reserve) {
    const reserve = await this.reserveModel.findByIdAndUpdate(id, data, {
      new: true,
    });
    return { reserve };
  }

  //#endregion
  //#region Delete
  @ApiOperation({ summary: 'حذف رزرو با شناسه' })
  @ApiParam({ name: 'id', description: 'شناسه' })
  @ApiOkResponse({ type: ReserveResponse })
  @Auth()
  @Delete(':id')
  async delete(@Param('id') id: string) {
    const reserve = await this.reserveModel.findByIdAndRemove(id);
    return { reserve };
  }

  //#endregion
}
