import { Module } from '@nestjs/common';
import { TypegooseModule } from 'nestjs-typegoose';
import { Reserve } from './../../models/reserve.model';
import { ReservesController } from './reserves.controller';

@Module({
  imports: [TypegooseModule.forFeature([Reserve])],
  controllers: [ReservesController],
})
export class ReservesModule {}
