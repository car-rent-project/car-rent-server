import {
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Query,
} from '@nestjs/common';
import { ApiOperation, ApiParam, ApiQuery, ApiTags } from '@nestjs/swagger';
import { ReturnModelType } from '@typegoose/typegoose';
import { InjectModel } from 'nestjs-typegoose';
import { State, STATE_TYPE } from './../../models/state.model';

@ApiTags('States')
@Controller('api/states')
export class StatesController {
  constructor(
    @InjectModel(State)
    private readonly stateModel: ReturnModelType<typeof State>,
  ) {}

  @ApiOperation({ summary: 'لیست استان ها و شهر ها' })
  @ApiQuery({
    required: true,
    name: 'type',
    enum: STATE_TYPE,
    description: 'نوع محل',
    examples: {
      province: { summary: 'استان', value: 'province' },
      city: { summary: 'شهر', value: 'city' },
    },
  })
  @ApiQuery({ required: false, name: 'parent', description: 'شناسه استان' })
  @Get()
  async find(@Query('type') type: string, @Query('parent') parent: string) {
    try {
      const states = await this.stateModel.find({ type, parent });
      const count = await this.stateModel.countDocuments({ type, parent });
      return { states, count };
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }

  @ApiOperation({ summary: 'استان یا شهر با شناسه' })
  @ApiParam({ name: 'id', description: 'شناسه' })
  @Get(':id')
  async findById(@Param('id') id: string) {
    try {
      const state = await this.stateModel.findById(id);
      return { state };
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }
}
