import { State } from './../../models/state.model';
import { Module } from '@nestjs/common';
import { StatesController } from './states.controller';
import { TypegooseModule } from 'nestjs-typegoose';

@Module({
  imports: [TypegooseModule.forFeature([State])],
  controllers: [StatesController],
})
export class StatesModule {}
