import {
  Body,
  Controller,
  HttpException,
  HttpStatus,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import {
  ApiBody,
  ApiConsumes,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { Request } from 'express';
import { diskStorage } from 'multer';
import * as path from 'path';

@ApiTags('Uploads')
@Controller('uploads')
export class UploadsController {
  //#region upload image
  @ApiOperation({ summary: 'آپلود تصویر' })
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'file',
          description: 'فایل تصویر',
        },
      },
    },
  })
  @ApiOkResponse({ schema: { properties: { url: { type: 'string' } } } })
  @Post('image')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: 'files/images',
        filename: (req, file, cb) => {
          // Generating a 32 random chars long string
          const randomName = Array(16)
            .fill(null)
            .map(() => Math.round(Math.random() * 16).toString(16))
            .join('');
          //Calling the callback passing the random name generated with the original extension name
          cb(
            null,
            `${Date.now()}-${randomName}${path.extname(file.originalname)}`,
          );
        },
      }),
      fileFilter: function (req: Request, file, callback) {
        const ext = path.extname(file.originalname);

        if (!['.png', '.jpg', '.jpeg'].some((e) => ext === e)) {
          req.body.error = 'نوع فایل باید تصویر باشد .';
          return callback(null, false);
        }

        if (file.size > 8 * 1024 * 1024) {
          req.body.error = 'اندازه فایل کمتر از 8 مگابایت باشد .';
          return callback(null, false);
        }

        callback(null, true);
      },
    }),
  )
  uploadImage(@UploadedFile('file') file, @Body('error') error: string) {
    try {
      if (!!error) throw error;
      return { url: `${file.destination}/${file.filename}` };
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }

  //#endregion
}
