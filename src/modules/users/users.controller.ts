import { UsersResponseDto } from './users.dto';
/* eslint-disable @typescript-eslint/no-inferrable-types */
import { Request } from 'express';
import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
  Req,
} from '@nestjs/common';
import { InjectModel } from 'nestjs-typegoose';
import {
  ApiBody,
  ApiOkResponse,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import * as jwt from 'jsonwebtoken';
import * as randomstring from 'randomstring';
import { JWT_SECRET } from 'src/config';
import { ApiGetQuery } from 'src/core/decorators';
import { Auth } from 'src/guards/auth.guard';
import { User } from 'src/models/user.model';
import { SmsService } from 'src/services/sms.service';
import {
  SignInBody,
  SignInResponse,
  UserResponseDto,
  VerifyBody,
  VerifyResponse,
} from './users.dto';
import { ReturnModelType } from '@typegoose/typegoose';

@ApiTags('Users')
@Controller('api/users')
export class UsersController {
  constructor(
    @InjectModel(User) private readonly userModel: ReturnModelType<typeof User>,
    private smsService: SmsService,
  ) {}

  //#region SignIn
  @ApiOperation({ summary: 'ورود کاربر' })
  @ApiBody({ type: SignInBody })
  @ApiOkResponse({ type: SignInResponse })
  @Post('signin')
  async signin(@Body('phone') phone: string): Promise<{ status: boolean }> {
    try {
      const userExist = await this.userModel.exists({ phone });

      const keys = {
        activateKey: randomstring.generate({
          charset: 'numeric',
          length: 6,
        }),
        activateExpire: new Date(Date.now() + 60 * 60 * 1000),
      };

      if (userExist) {
        await this.userModel.findOneAndUpdate(
          { phone },
          { keys },
          { new: true },
        );
      } else {
        const user = new this.userModel({ phone, keys });
        await user.save();
      }

      const activationKey = keys.activateKey;
      this.smsService.send(phone, activationKey);

      return { status: true };
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }

  // #endregion
  //#region Verify
  @ApiOperation({ summary: 'تایید کاربر' })
  @ApiBody({ type: VerifyBody })
  @ApiResponse({ type: VerifyResponse })
  @Post('verify')
  async verify(@Body('phone') phone: string, @Body('key') key: string) {
    try {
      const user = await this.userModel.findOne({
        phone,
        'keys.activateKey': key,
      });

      if (user) {
        if (user.keys.activateExpire <= new Date()) {
          throw { message: 'درخواست شما منقضی شده است .' };
        }

        user.keys.activateKey = '';
        user.keys.activateExpire = new Date(Date.now());
        user.status = 'active';
        await user.save();

        const payload = { _id: user._id };
        const token = jwt.sign(payload, JWT_SECRET);

        return { message: 'شما وارد شدید !', token, id: user._id };
      } else {
        throw { message: 'کد فعال سازی اشتباه است .' };
      }
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }

  // #endregion
  //#region Get profile
  @ApiOperation({ summary: 'دریافت پروفایل' })
  @ApiOkResponse({ type: UserResponseDto })
  @Auth()
  @Get('profile')
  async profile(@Req() req: { user: User }): Promise<UserResponseDto> {
    try {
      const user = await this.userModel.findById(
        { _id: req.user._id },
        '-keys -status -phone',
      );
      return { user };
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }

  //#endregion
  //#region Update profile
  @ApiOperation({ summary: 'بروزرسانی پروفایل ' })
  @ApiBody({ type: User })
  @ApiOkResponse({ type: User })
  @Auth()
  @Put('profile')
  async updateProfile(
    @Req() req: { user: User },
    @Body() data: User,
  ): Promise<UserResponseDto> {
    try {
      const user = await this.userModel.findByIdAndUpdate(req.user._id, data, {
        new: true,
      });
      return { user };
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }

  // #endregion
  //#region Create user
  @ApiOperation({ summary: 'ایجاد کاربر' })
  @ApiOkResponse({ type: UserResponseDto })
  @Auth('admin')
  @Post()
  async createUser(@Body() data: User): Promise<UserResponseDto> {
    try {
      const user = new this.userModel(data);
      await user.save();
      return { user };
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }

  // #endregion
  //#region Users list
  @ApiOperation({ summary: 'لیست کاربران' })
  @ApiGetQuery()
  @ApiOkResponse({ type: UsersResponseDto })
  @Auth('admin')
  @Get()
  async getUsersList(
    @Query('search') search: string,
    @Query('role') role: string,
    @Query('skip') skip: number = 0,
    @Query('limit') limit: number = 10,
  ): Promise<{ users: User[]; count: number }> {
    try {
      const query = {};

      if (!!search) {
        Object.assign(query, { name: new RegExp(search, 'ig') });
      }

      if (!!role) {
        Object.assign(query, { role });
      }

      const users = await this.userModel
        .find(query)
        .skip(Number(skip.toString()))
        .limit(Number(limit.toString()));

      const count = await this.userModel.countDocuments(query);

      return { users, count };
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }
  // #endregion
  //#region User single
  @ApiOperation({ summary: 'دریافت کاربر با شناسه' })
  @ApiOkResponse({ type: UserResponseDto })
  @Auth('admin')
  @Get(':id')
  async getUser(@Param('id') id: string): Promise<UserResponseDto> {
    try {
      const user = await this.userModel.findById(id, '-keys');
      return { user };
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }
  //#endregion
  //#region Users update
  @ApiOperation({ summary: 'بروزرسانی کاربر' })
  @ApiBody({ type: User })
  @ApiOkResponse({ type: User })
  @Auth('admin')
  @Put()
  async updateUser(@Body() data: User) {
    try {
      const user = await this.userModel.findByIdAndUpdate(data._id, data);
      return { user };
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }
  //#endregion
}
