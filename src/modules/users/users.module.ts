import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { SmsService } from 'src/services/sms.service';
import { TypegooseModule } from 'nestjs-typegoose';
import { User } from '../../models/user.model';

@Module({
  imports: [TypegooseModule.forFeature([User])],
  controllers: [UsersController],
  providers: [SmsService],
})
export class UsersModule {}
